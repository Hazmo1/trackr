import 'dart:developer';

import 'package:flutter/material.dart';

// ignore: import_of_legacy_library_into_null_safe
import 'package:horizontal_picker/horizontal_picker.dart';
import 'package:provider/provider.dart';
import 'package:trackr/model/Weight.dart';
import 'package:trackr/provider/database/WeightProvider.dart';

class HomeScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Welcome to Flutter'),
        ),
        body: Center(
          child: Consumer<WeightProvider>(
            builder: (context, provider, child) => weightListView(provider),
          ),
        ),
        floatingActionButton: FloatingActionButton(
            child: Icon(Icons.add),
            onPressed: () async
            {
              await showDialog(
                  context: context,
                  builder: (BuildContext context) {
                    late double weight;
                    return AlertDialog(
                      title: Text("Select weight"),
                      content: StatefulBuilder(builder:
                          (BuildContext context, StateSetter setState) {
                        return HorizantalPicker(
                          minValue: 100.0,
                          maxValue: 600.0,
                          divisions: 5000,
                          onChanged: (changedWeight) {
                            setState(() => weight = changedWeight);
                          },
                        );
                      }),
                      actions: [
                        TextButton(
                            onPressed: () {
                              Provider.of<WeightProvider>(context, listen: false).addWeight(Weight(weight));
                              Navigator.pop(context, "Ok");
                            },
                            child: Text("Hello", textAlign: TextAlign.end))
                      ],
                    );
                  });
            }));
  }
}

class WeightDialog extends StatefulWidget {
  const WeightDialog({Key? key}) : super(key: key);

  @override
  _WeightDialogState createState() => _WeightDialogState();
}

class _WeightDialogState extends State<WeightDialog> {
  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: Text("Select weight"),
      content: HorizantalPicker(
          minValue: 100.0,
          maxValue: 600.0,
          divisions: 5000,
          onChanged: (weight) {}),
      actions: [
        TextButton(
            onPressed: () {}, child: Text("Hello", textAlign: TextAlign.end))
      ],
    );
  }
}

Widget weightListView(WeightProvider provider) {
  log("in list view ${provider.weights.length}");
  if (provider.weights.length == 0) return CircularProgressIndicator();
  return ListView.separated(
    padding: const EdgeInsets.all(8),
    itemBuilder: (BuildContext context, int index) {
      return ListTile(
        title: Text(provider.weights[index].weight.toString()),
        subtitle: Text(provider.weights[index].date.toString()),
      );
    },
    separatorBuilder: (BuildContext context, int index) => const Divider(),
    itemCount: provider.weights.length,
  );
}
