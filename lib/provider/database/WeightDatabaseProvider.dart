import 'dart:developer';

import 'package:flutter/foundation.dart';
import 'package:sqflite/sqflite.dart' as sql;
import 'package:path/path.dart' as path;

class WeightDatabaseProvider with ChangeNotifier {
  static const PATH = 'weight.db';
  static const TABLE_NAME = 'weight';
  sql.Database ?database;

  WeightDatabaseProvider() {
    init();
    log("testing if this is called first");
  }

  void init() async {
    final dbPath = await sql.getDatabasesPath();
    database = await sql.openDatabase(
      path.join(dbPath, PATH),
      onCreate: (db, version) {
        final statement = '''CREATE TABLE IF NOT EXISTS $TABLE_NAME (
              id INTEGER PRIMARY KEY,
              weight REAL,
              date INTEGER
            )'''
            .trim();
        return db.execute(statement);
      },
      version: 1,
    );
    notifyListeners();
    log("done initing the database ${DateTime.now()}");
  }

  Future<void> insert(Map<String, Object> values) async {
    await database!.insert(TABLE_NAME, values);
  }

  Future<List<Map<String, Object?>>> getAll() async {
    return await database!.query(TABLE_NAME);
  }
}
