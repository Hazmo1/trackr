import 'dart:developer';

import 'package:flutter/foundation.dart';
import 'package:trackr/model/Weight.dart';
import 'package:trackr/provider/database/WeightDatabaseProvider.dart';

class WeightProvider with ChangeNotifier {
  WeightDatabaseProvider? _weightDatabaseProvider;
  List<Weight> _weights;

  List<Weight> get weights => new List.from(_weights.reversed);

  WeightProvider(this._weightDatabaseProvider, this._weights) {
    if (_weightDatabaseProvider != null) {
      _getAllAndSetWeights();
    }
  }

  void addWeight(Weight weight) {
    if (_weightDatabaseProvider!.database != null) {
      _weights.add(weight);
      notifyListeners();
      _weightDatabaseProvider!.insert(weight.toMap());
    }
  }

  Future<void> _getAllAndSetWeights() async {
    if (_weightDatabaseProvider!.database != null) {
      final allWeights = await _weightDatabaseProvider!.getAll();
      _weights = allWeights.map((weight) => Weight.fromMap(weight)).toList();
      notifyListeners();
      log("bye ${DateTime.now()}");
    }
  }
}