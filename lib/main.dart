import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:trackr/provider/database/WeightDatabaseProvider.dart';
import 'package:trackr/provider/database/WeightProvider.dart';
import 'package:trackr/screens/HomeScreen.dart';

void main() => runApp(MultiProvider(providers: [
      ChangeNotifierProvider(create: (_) => WeightDatabaseProvider()),
      ChangeNotifierProxyProvider<WeightDatabaseProvider, WeightProvider>(
          create: (_) => WeightProvider(null, []),
          update: (_, db, previous) => WeightProvider(db, previous!.weights))
    ], child: MyApp()));

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(title: 'Welcome to Flutter', home: HomeScreen());
  }
}
