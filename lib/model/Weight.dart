class Weight {
  double weight;
  DateTime date;

  Weight(this.weight) : date = DateTime.now();

  Weight.fromMap(Map<String, dynamic> data) :
    weight = data['weight'],
    date = DateTime.fromMillisecondsSinceEpoch(data['date']);

  Map<String, Object> toMap() {
    return {
      'weight': weight,
      'date': date.millisecondsSinceEpoch
    };
  }
}